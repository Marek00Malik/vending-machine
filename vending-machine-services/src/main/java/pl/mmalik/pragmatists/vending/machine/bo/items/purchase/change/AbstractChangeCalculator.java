package pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change;

import com.beust.jcommander.internal.Lists;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;

import java.math.BigDecimal;
import java.util.List;

import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.isBiggerOrEqual;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.isBiggerThen;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.WalletUtils.findSmallestValue;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.WalletUtils.getPossibleCoins;

/**
 * Created by Marek on 23.03.2016.
 */
public abstract class AbstractChangeCalculator {

    public List<Coins> calculateChange(BigDecimal change){
        List<Coins> returnCoins = Lists.newArrayList();

        while (isBiggerThen(change, BigDecimal.ZERO) &&
                isBiggerThen(getWallet().getTotal(), BigDecimal.ZERO)) {
            Coins coins = bestFit(change);

            change = subtractCoinsFromRemainingChange(change, coins);
            returnCoins.add(coins);
        }

        return returnCoins;
    }

    protected Coins bestFit(BigDecimal change) {
        CoinType prepType = findSmallestValue(getWallet());

        for(CoinType currType : getPossibleCoins(getWallet())){
            if(isBiggerOrEqual(change, currType.value()) &&
                    isBiggerThen(currType.value(), prepType.value())){
                prepType = currType;
            }
        }

        int maxPossibleCoinsCnt = change.divide(prepType.value()).intValue();

        return getWallet()
                .takeCoins(maxPossibleCoinsCnt, prepType);
    }

    protected BigDecimal subtractCoinsFromRemainingChange(BigDecimal change, Coins coins) {
        return change
                .subtract(coins.getCoinType().value()
                        .multiply(BigDecimal.valueOf(coins.getCount())));
    }

    protected abstract Wallet getWallet();
}
