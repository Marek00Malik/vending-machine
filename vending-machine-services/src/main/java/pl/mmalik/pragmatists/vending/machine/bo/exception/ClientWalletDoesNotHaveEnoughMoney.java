package pl.mmalik.pragmatists.vending.machine.bo.exception;

import java.math.BigDecimal;

/**
 * Created by Marek on 21.03.2016.
 */
public class ClientWalletDoesNotHaveEnoughMoney extends Exception {
    public ClientWalletDoesNotHaveEnoughMoney(Long itemId, BigDecimal remainingMoney) {
        super("Not Enough money to pay for item - " + itemId + "(Remains " + remainingMoney + ")");
    }
}
