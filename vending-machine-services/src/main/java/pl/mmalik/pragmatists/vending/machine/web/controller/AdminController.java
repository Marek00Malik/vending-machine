package pl.mmalik.pragmatists.vending.machine.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mmalik.pragmatists.vending.machine.bo.VendingMachineWallet;
import pl.mmalik.pragmatists.vending.machine.bo.dto.AdminCheckReturnDto;

/**
 * Created by Marek on 31.03.2016.
 */
@RestController
@RequestMapping("/rest/admin")
public class AdminController {

    @Autowired
    private VendingMachineWallet vendingMachineWallet;

    @RequestMapping(path = "/machine-check", method = RequestMethod.GET)
    public ResponseEntity<AdminCheckReturnDto> getMachineStatus(){
        return new ResponseEntity<AdminCheckReturnDto>(new AdminCheckReturnDto(vendingMachineWallet.get()), HttpStatus.OK);
    }

}
