/**
 * Created by Marek on 15.03.2016.
 */
angular.module('app').factory('AvailableItemsService', function ($http) {
    var getItems = function () {
        return $http.get('/rest/all-items');
    };

    return {getItems: getItems};
});
