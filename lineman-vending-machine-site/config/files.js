/* Exports a function which returns an object that overrides the default &
 *   plugin file patterns (used widely through the app configuration)
 *
 * To see the default definitions for Lineman's file paths and globs, see:
 *
 *   - https://github.com/linemanjs/lineman/blob/master/config/files.coffee
 */
module.exports = function (lineman) {
    //Override file patterns here
    return {
        js: {
            app: [
                "app/js/angular_app.js",
                "app/js/**/*.js"
            ],
            vendor: [
                "vendor/js/angular.js",
                "vendor/js/**/*.js"
            ]
        },

        webfonts: {
            vendor: [
                'vendor/webfonts/*.ttf'
            ]
        },

        less: {
            compile: {
                options: {
                    paths: [
                        "app/css/**/*.css",
                        "app/css/**/*.less",
                        "vendor/css/bootstrap.css",
                        "vendor/css/normalize.css",
                        "vendor/css/**/*.css"
                    ]
                }
            }
        }
    };
};
