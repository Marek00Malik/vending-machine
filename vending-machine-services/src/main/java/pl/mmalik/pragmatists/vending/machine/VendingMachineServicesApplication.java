package pl.mmalik.pragmatists.vending.machine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingMachineServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(VendingMachineServicesApplication.class, args);
    }
}
