package pl.mmalik.pragmatists.vending.machine.bo.dto;

import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;
import pl.mmalik.pragmatists.vending.machine.bo.builder.CoinsBuilder;

/**
 * Dto representing coins of a given nominal value;
 * Created by Marek on 20.03.2016.
 */
public class Coins {
    private CoinType coinType;
    private int count;

    public CoinType getCoinType() {
        return coinType;
    }

    public void setCoinType(CoinType coinType) {
        this.coinType = coinType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Add another coinType.
     */
    public void addOne(){
        count++;
    }

    /**
     * Add coins by param value.
     * @param add
     */
    public void add(int add){
        count += add;
    }

    /**
     * Returns one coin.
     * @return
     *  true if after removal there are no more coins.
     */
    public boolean removeOne(){
        count--;
        return count == 0;
    }

    /**
     *@return
     * Returns CoinsBuilder.
     */
    public static CoinsBuilder builder(){
        return new CoinsBuilder();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Coins){
            Coins obj = (Coins) o;
            return this.getCoinType().equals(obj.getCoinType());
        }

        return false;
    }

    @Override
    public int hashCode(){
        return coinType.hashCode() + count;
    }
}
