package pl.mmalik.pragmatists.vending.machine.bo.items.available;


import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage;
import pl.mmalik.pragmatists.vending.machine.bo.dto.VendingMachineRow;
import pl.mmalik.pragmatists.vending.machine.domain.repository.StorageRepository;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Component retrieving all available items.
 * <p>
 * Created by Marek on 19.03.2016.
 */
@Component
public class AvailableItemsBoImpl {

    private static final Character[] rows = new Character[]{'A', 'B', 'C', 'D', 'E', 'F'};

    @Autowired
    private StorageRepository storageRepository;

    /**
     * @return List {@link VendingMachineRow} (s) of possible items to choose from.
     */
    public List<VendingMachineRow> retrieveAvailableItems() {
        LinkedList<VendingMachineRow> result = Lists.newLinkedList();
        storageRepository.getStoredItems()
                .stream()
                .forEach(storedItem -> addToList(storedItem, result));

        return result;
    }

    private void addToList(ItemInStorage item, LinkedList<VendingMachineRow> result) {
        VendingMachineRow row = getRow(result, item);

        row.getItems().put(item.getColumnNumber(), item);
    }

    private VendingMachineRow getRow(List<VendingMachineRow> result, ItemInStorage item) {
        if(CollectionUtils.isEmpty(result) || rowNumberMissing(result, item.getRowNumber())){
            return createNextRow(result, item.getRowNumber());
        } else {
            return findRowWithNumber(result, item);
        }
    }

    private VendingMachineRow findRowWithNumber(List<VendingMachineRow> result, ItemInStorage item) {
        return result.stream()
                .filter(row -> row.getRowNo().equals(rows[item.getRowNumber()]))
                .collect(toList())
                .get(0);
    }

    private boolean rowNumberMissing(List<VendingMachineRow> result, Integer rowNumber) {
        return result.stream()
                .filter(row -> row.getRowNo().equals(rows[rowNumber]))
                .count() == 0;
    }

    private VendingMachineRow createNextRow(List<VendingMachineRow> result, int rowNo) {
        VendingMachineRow newRow = new VendingMachineRow(rows[rowNo]);
        result.add(newRow);
        return newRow;
    }
}
