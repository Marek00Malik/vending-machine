package pl.mmalik.pragmatists.vending.machine.bo.dto;

/**
 * Return object for Admin Page.
 * Created by Marek on 31.03.2016.
 */
public class AdminCheckReturnDto {
    private Wallet wallet;

    public AdminCheckReturnDto(Wallet wallet) {
        this.wallet = wallet;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
