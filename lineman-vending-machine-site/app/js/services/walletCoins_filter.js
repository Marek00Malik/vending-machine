/**
 * Created by mmalik on 2016-03-28.
 */
angular.module('app').factory('WalletSelectedCoinsJSFilter', function () {
    var filter = function (coins) {
        var selectedCoins = [];
        coins.map(function (obj) {
            angular.forEach(obj, function (value, key) {
                if (value > 0) {
                    selectedCoins.push(obj);
                }
            });
        });

        return selectedCoins;
    };

    return {filter: filter};
});