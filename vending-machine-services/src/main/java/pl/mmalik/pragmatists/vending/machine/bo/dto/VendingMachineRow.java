package pl.mmalik.pragmatists.vending.machine.bo.dto;

import com.google.common.collect.Maps;
import pl.mmalik.pragmatists.vending.machine.bo.builder.VendingMachineRowBuilder;

import java.util.Map;

/**
 * Dto passed to the web client representing a single vending machine shelf.
 * Created by Marek on 19.03.2016.
 */
public class VendingMachineRow {
    private Character rowNo;
    private Map<Integer, ItemInStorage> items;

    public VendingMachineRow() {
        items = Maps.newHashMap();
        items.put(0, null);
        items.put(1, null);
        items.put(2, null);
        items.put(3, null);
    }

    public VendingMachineRow(Character rowNo) {
        this();
        this.rowNo = rowNo;
    }

    public static VendingMachineRowBuilder builder() {
        return new VendingMachineRowBuilder();
    }

    public Character getRowNo() {
        return rowNo;
    }

    public void setRowNo(Character rowNo) {
        this.rowNo = rowNo;
    }

    public Map<Integer, ItemInStorage> getItems() {
        return items;
    }

    public void setItems(Map<Integer, ItemInStorage> items) {
        this.items = items;
    }
}
