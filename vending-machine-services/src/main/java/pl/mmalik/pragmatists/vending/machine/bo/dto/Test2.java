package pl.mmalik.pragmatists.vending.machine.bo.dto;

import java.math.BigDecimal;

public class Test2 extends Test1 {
    int k = 5;

    public BigDecimal m2(int x) {
        return BigDecimal.valueOf(++x);
    }

    public static void main(String[] args) {
        Test2 test2 = new Test2();
        System.out.println(test2.test(1));
        char c = Short.MAX_VALUE;
        System.out.println(c);
        System.out.println(c == Short.MAX_VALUE);



        int i = 12234509;
        Integer i1 = 1;
        Integer i2 = new Integer(1);

        float f = i;
        Long l = 1L;
        Byte b1 = 1;
        System.out.println(b1.equals(i));
        System.out.println(i2.equals(l));
        short s = 1;
        long l1 = s;

        System.out.println();
        System.out.println("After loop");

        switch (i){
            case 1:
                i1 = 1;
                break;
            default:
                i1 = -1;
                break;
            case 2:
                i1 = i;
                break;
        }
    }
    static {
        throw new NullPointerException();
    }
}