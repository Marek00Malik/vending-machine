/**
 * Created by Marek on 17.03.2016.
 */
angular.module('app').controller('EnterCoinsModalController', function ($scope, $filter, $uibModalInstance, wallet) {
    $scope.title = 'Please enter Your change.';
    $scope.wallet = wallet;

    $scope.enterCash = function (coins) {
        $scope.wallet.coins.map(function(obj){
           if(obj.hasOwnProperty(coins)){
                obj[coins] += 1;
           }
        });
        coins = coins.toFixed(2);
        $scope.wallet.total += parseFloat(coins);
        $scope.wallet.total = parseFloat($scope.wallet.total.toFixed(2));
    };

    $scope.ok = function () {
        $uibModalInstance.close($scope.wallet);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss(0);
    };

});