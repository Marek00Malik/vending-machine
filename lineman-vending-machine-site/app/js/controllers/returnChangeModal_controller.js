/**
 * Created by Marek on 31.03.2016.
 */
angular.module('app').controller('ReturnChangeModalController', function ($scope, $uibModalInstance, coins, returnMessage) {
    $scope.coins = coins;
    $scope.returnMessage = returnMessage;
    $scope.title = 'Clients return change';

    $scope.ok = function () {
        $uibModalInstance.close();
    };
});
