package pl.mmalik.pragmatists.vending.machine.bo;

import com.google.common.collect.Lists;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage;
import pl.mmalik.pragmatists.vending.machine.bo.dto.VendingMachineRow;
import pl.mmalik.pragmatists.vending.machine.bo.items.available.AvailableItemsBoImpl;
import pl.mmalik.pragmatists.vending.machine.domain.repository.StorageRepository;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.toBigDec;

/**
 * Created by Marek on 19.03.2016.
 */
public class AvailableItemsBoImplTest {

    @InjectMocks
    private AvailableItemsBoImpl sut;

    @Mock
    private StorageRepository storageRepository;

    @BeforeMethod
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(dataProvider = "correctData")
    public void testConversion(List<ItemInStorage> items, int rows) {

        //given

        //when
        when(storageRepository.getStoredItems()).thenReturn(items);

        //then
        List<VendingMachineRow> vendingMachineRows = sut.retrieveAvailableItems();


        assertEquals(vendingMachineRows.size(), rows);

        ItemInStorage itemInStorage = items.get(5);
        ItemInStorage sortedItem =
                vendingMachineRows.get(itemInStorage.getRowNumber())
                        .getItems()
                        .get(itemInStorage.getColumnNumber());
        assertEquals(sortedItem, itemInStorage);

        ItemInStorage itemInStorage1 = items.get(7);
        ItemInStorage sortedItem1 =
                vendingMachineRows.get(itemInStorage1.getRowNumber())
                        .getItems()
                        .get(itemInStorage1.getColumnNumber());
        assertEquals(sortedItem1, itemInStorage1);
    }

    @DataProvider(name = "correctData")
    public Object[][] getCorrectData() {
        return new Object[][]{
                {Lists.newArrayList(
                        item("CocaCola", 2.5, .33, 2, 0),
                        item("Fanta", 2.99, .33, 3, 1),
                        item("Pepsi", 2.49, .33, 3, 2),
                        item("Cherry Coca", 2.89, .33, 2, 3),

                        item("Woda", 1.69, .33, 2, 4),
                        item("Sok Tymbark Pomarańczowy", 2.49, .25, 5, 5),
                        item("Sok Tymbark Jablko-Mieta", 2.69, .25, 5, 6),

                        item("Snikers", 1.69, 0, 3, 8)),
                        3
                }
        };
    }

    private ItemInStorage item(String name, double cost, double volume, int stored, Integer order) {
        return ItemInStorage.builder()
                .name(name)
                .valueCost(toBigDec(cost))
                .volume(toBigDec(volume))
                .howMany(stored)
                .order(order)
                .build();
    }
}
