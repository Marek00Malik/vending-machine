package pl.mmalik.pragmatists.vending.machine.bo.exception;

import java.math.BigDecimal;

/**
 * Created by Marek on 30.03.2016.
 */
public class CoinTypeNotFound extends RuntimeException {
    public CoinTypeNotFound(BigDecimal decimal) {
        super("Could not find Coin Type with value " + decimal.toPlainString());
    }
}
