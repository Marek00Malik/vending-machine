package pl.mmalik.pragmatists.vending.machine.bo.builder;

import pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage;

import java.math.BigDecimal;

/**
 * Builder for ItemInStorage.
 * Created by Marek on 24.03.2016.
 */
public class ItemInStorageBuilder {
    private ItemInStorage that;

    public ItemInStorageBuilder() {
        that = new ItemInStorage();
    }

    public ItemInStorageBuilder id(Long id) {
        this.that.setId(id);
        return this;
    }

    public ItemInStorageBuilder name(String name) {
        this.that.setName(name);
        return this;
    }

    public ItemInStorageBuilder valueCost(BigDecimal valueCost) {
        this.that.setValueCost(valueCost);
        return this;
    }

    public ItemInStorageBuilder volume(BigDecimal volume) {
        this.that.setVolume(volume);
        return this;
    }

    public ItemInStorageBuilder order(Integer order) {
        this.that.setOrder(order);
        return this;
    }

    public ItemInStorageBuilder howMany(Integer howMany) {
        this.that.setHowMany(howMany);
        return this;
    }

    public ItemInStorage build() {
        return that;
    }
}
