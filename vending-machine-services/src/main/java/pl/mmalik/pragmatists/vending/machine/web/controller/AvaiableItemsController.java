package pl.mmalik.pragmatists.vending.machine.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.mmalik.pragmatists.vending.machine.bo.items.available.AvailableItemsBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.dto.VendingMachineRow;

import java.util.List;

/**
 * Controller used to retrieve all available items for customer at the vending machine.
 * Created by Marek on 19.03.2016.
 */
@RestController
@RequestMapping(path = "/rest/all-items")
public class AvaiableItemsController {

    @Autowired
    private AvailableItemsBoImpl availableItemsBoImpl;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<VendingMachineRow> getAvaiableItems(){
        return availableItemsBoImpl.retrieveAvailableItems();
    }
}
