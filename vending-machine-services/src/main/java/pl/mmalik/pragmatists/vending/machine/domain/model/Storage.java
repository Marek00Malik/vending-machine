package pl.mmalik.pragmatists.vending.machine.domain.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by Marek on 19.03.2016.
 */
@Table
@Entity
public class Storage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Item itemInStore;

    @Column(name = "ITEMS_AVAILABLE")
    private Integer itemsAvailable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItemInStore() {
        return itemInStore;
    }

    public void setItemInStore(Item itemInStore) {
        this.itemInStore = itemInStore;
    }

    public Integer getItemsAvailable() {
        return itemsAvailable;
    }

    public void setItemsAvailable(Integer itemsAvailable) {
        this.itemsAvailable = itemsAvailable;
    }
}
