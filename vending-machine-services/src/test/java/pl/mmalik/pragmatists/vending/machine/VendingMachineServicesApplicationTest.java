package pl.mmalik.pragmatists.vending.machine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Test Configuration:
 * ddl-auto=update
 */
@SpringBootApplication
public class VendingMachineServicesApplicationTest {

	public static void main(String[] args) {
        SpringApplication.run(VendingMachineServicesApplication.class, args);
	}
}
