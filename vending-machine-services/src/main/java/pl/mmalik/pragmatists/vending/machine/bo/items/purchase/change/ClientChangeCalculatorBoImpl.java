package pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mmalik.pragmatists.vending.machine.bo.VendingMachineWallet;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;

/**
 * BO used to calculate the change that will be given to the client.
 * Created by Marek on 21.03.2016.
 */
@Component
public class ClientChangeCalculatorBoImpl extends AbstractChangeCalculator {

    @Autowired
    private VendingMachineWallet vendingMachineWallet;

    @Override
    protected Wallet getWallet() {
        return vendingMachineWallet.get();
    }
}
