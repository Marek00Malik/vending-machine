/* Define custom server-side HTTP routes for lineman's development server
 *   These might be as simple as stubbing a little JSON to
 *   facilitate development of code that interacts with an HTTP service
 *   (presumably, mirroring one that will be reachable in a live environment).
 *
 * It's important to remember that any custom endpoints defined here
 *   will only be available in development, as lineman only builds
 *   static assets, it can't run server-side code.
 *
 * This file can be very useful for rapid prototyping or even organically
 *   defining a spec based on the needs of the client code that emerge.
 *
 */

module.exports = {
    drawRoutes: function (app) {
        app.get('/test/all-items', function (req, res) {
            res.json([
                {
                    rowNo: 'A',
                    items: [
                        {id: 1, name: 'Coca Cola', quantity: 2, volume: '0.3L', value: '2.5'},
                        {id: 5, name: 'Tymbark Pomaranczowy', quantity: 2, volume: '0.22L', value: '2'},
                        {id: 7, name: 'Woda', quantity: 2, volume: '0.3L', value: '1.6'},
                        {id: 2, name: 'Fanta', quantity: 4, volume: '0.35L', value: '2.8'}
                    ]
                },
                {
                    rowNo: 'B',
                    items: [
                        {id: 10, name: 'Lejsy', quantity: 2, value: '2.75'},
                        {id: 12, name: 'Draże kokoswoe', quantity: 2, value: '1.35'},
                        {id: 9, name: 'Snikers', quantity: 2, value: '1.75'}
                    ]
                }
            ]);
        });
    }
};
