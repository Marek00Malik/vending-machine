package pl.mmalik.pragmatists.vending.machine.bo.utils;

import java.math.BigDecimal;

/**
 * Created by Marek on 19.03.2016.
 */
public final class BigDecimalUtils {
    public static final int SCALE = 2;

    private BigDecimalUtils() {
    }

    /**
     * @param value
     * @return converted double to BigDecimal value.
     */
    public static BigDecimal toBigDec(final double value) {
        BigDecimal bigDecimal = BigDecimal.valueOf(value);
        bigDecimal = bigDecimal.setScale(SCALE);
        return bigDecimal;
    }

    /**
     * @param value
     * @return
     * converts string representation to BigDecimal with scale.
     */
    public static BigDecimal toBigDec(String value) {
        return toBigDec(Double.parseDouble(value));
    }

    /**
     * Checking for a > b.
     * @param a
     * @param b
     * @return is a bigger then b.
     */
    public static boolean isBiggerThen(BigDecimal a, BigDecimal b){
        return a.compareTo(b) == 1;
    }

    /**
     * Checking for a >= b.
     * @param a
     * @param b
     * @return is a bigger then or equal b.
     */
    public static boolean isBiggerOrEqual(BigDecimal a, BigDecimal b){
        return a.compareTo(b) >= 0;
    }

    /**
     * Checks for a==b.
     * @param a
     * @param b
     * @return are values A and B equal;
     */
    public static boolean areEqual(BigDecimal a, BigDecimal b){
        return a.compareTo(b) == 0;
    }
}
