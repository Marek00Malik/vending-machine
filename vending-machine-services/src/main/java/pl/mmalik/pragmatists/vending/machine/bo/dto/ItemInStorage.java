package pl.mmalik.pragmatists.vending.machine.bo.dto;

import pl.mmalik.pragmatists.vending.machine.bo.builder.ItemInStorageBuilder;

import java.math.BigDecimal;

/**
 * Created by Marek on 19.03.2016.
 */
public class ItemInStorage {
    private final int ROW_LIMIT = 4;

    private Long id;

    private String name;

    private BigDecimal valueCost;

    private BigDecimal volume;

    private Integer order;

    private Integer howMany;

    public ItemInStorage(){}

    public ItemInStorage(Long id, String name, BigDecimal valueCost, BigDecimal volume, Integer order, Integer howMany){
        this.id = id;
        this.name = name;
        this.valueCost = valueCost;
        this.volume = volume;
        this.order = order;
        this.howMany = howMany;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValueCost() {
        return valueCost;
    }

    public void setValueCost(BigDecimal valueCost) {
        this.valueCost = valueCost;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getHowMany() {
        return howMany;
    }

    public void setHowMany(Integer howMany) {
        this.howMany = howMany;
    }

    public int getRowNumber(){
        return order/ROW_LIMIT;
    }

    public int getColumnNumber(){
        return order%ROW_LIMIT;
    }

    public static ItemInStorageBuilder builder(){
        return new ItemInStorageBuilder();
    }

    @Override
    public String toString() {
        return "ItemInStorage{" +
                "name='" + name + '\'' +
                ", valueCost=" + valueCost +
                ", volume=" + volume +
                '}';
    }
}
