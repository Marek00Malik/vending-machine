package pl.mmalik.pragmatists.vending.machine.bo.exception;

import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;

/**
 * Exception when an attempt to remove Coins nominal from wallet when there still are spare to use.
 * Created by Marek on 22.03.2016.
 */
public class CoinsStillAvailableException extends RuntimeException {
    /**
     *  Exception when an attempt to remove Coins nominal from wallet when there still are spare to use.
     * @param coins
     */
    public CoinsStillAvailableException(Coins coins) {
        super("Coins in nominal["+ coins.getCoinType().name() +"] still are in quantity = " + coins.getCount());
    }
}
