package pl.mmalik.pragmatists.vending.machine.bo.items.purchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseRequestDto;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseReturnDto;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ClientWalletDoesNotHaveEnoughMoney;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ItemNotAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.exception.NotEnoughMoneyException;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change.ClientChangeCalculatorBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.validate.PurchaseValidator;
import pl.mmalik.pragmatists.vending.machine.domain.repository.ItemRepository;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Created by Marek on 20.03.2016.
 */
@Component
public class PurchaseBoImpl {

    private static final String MESSAGE = "No details where passed, please try again, "
            + "if problem will continue contact administrators.";

    @Autowired
    private List<PurchaseValidator> purchaseValidators;

    @Autowired
    private ClientChangeCalculatorBoImpl changeCalculatorBo;

    @Autowired
    private ItemRepository repository;

    /**
     * Purchase a given product listed in param, that should be paid for with clients wallet (coins).
     * @param purchaseRequest
     * @return
     *  returns {@link PurchaseReturnDto} object with response message and return change.
     * @throws ClientWalletDoesNotHaveEnoughMoney - thrown when client has not enough money.
     * @throws NotEnoughMoneyException - thrown then vending machine has not enough money to return change.
     * @throws ItemNotAvailableException - thrown then selected item is not available in stock anymore.
     */
    public PurchaseReturnDto purchaseItem(PurchaseRequestDto purchaseRequest)
            throws ClientWalletDoesNotHaveEnoughMoney, NotEnoughMoneyException, ItemNotAvailableException {
        validatePurchase(purchaseRequest);

        BigDecimal itemCost = repository.findOne(purchaseRequest.getItemId()).getValueCost();
        BigDecimal changeToReturn = purchaseRequest.getClientWallet().getTotal().subtract(itemCost);
        List<Coins> returnChange = changeCalculatorBo.calculateChange(changeToReturn);
        return PurchaseReturnDto.builder()
                .coins(returnChange)
                .returnMessage(changeToReturn.toPlainString())
                .build();
    }

    private void validatePurchase(PurchaseRequestDto purchaseRequest)
            throws ItemNotAvailableException, ClientWalletDoesNotHaveEnoughMoney, NotEnoughMoneyException {
        requireNonNull(purchaseRequest.getItemId(), MESSAGE);
        requireNonNull(purchaseRequest.getClientWallet(), MESSAGE);

        for (PurchaseValidator validator : purchaseValidators) {
            validator.validate(purchaseRequest);
        }
    }

}
