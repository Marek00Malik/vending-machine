package pl.mmalik.pragmatists.vending.machine;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Marek on 19.03.2016.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"pl.mmalik.pragmatists.vending.machine.domain.repository"})
public class SpringJPAConfig {
}
