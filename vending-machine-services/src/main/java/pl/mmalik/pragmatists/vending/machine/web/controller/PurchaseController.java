package pl.mmalik.pragmatists.vending.machine.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.PurchaseBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseRequestDto;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseReturnDto;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ClientWalletDoesNotHaveEnoughMoney;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ItemNotAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.exception.NotEnoughMoneyException;

/**
 * Created by Marek on 20.03.2016.
 */
@RestController
@RequestMapping(path = "/rest/purchase-item")
public class PurchaseController {

    @Autowired
    private PurchaseBoImpl purchaseBo;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PurchaseReturnDto> purchaseItem(@RequestBody PurchaseRequestDto clientRequest) {

        try {
            PurchaseReturnDto purchaseReturnDto = purchaseBo.purchaseItem(clientRequest);
            return new ResponseEntity<PurchaseReturnDto>(purchaseReturnDto, HttpStatus.OK);
        } catch (ClientWalletDoesNotHaveEnoughMoney |
                ItemNotAvailableException |
                NotEnoughMoneyException e) {
            PurchaseReturnDto returnDto = new PurchaseReturnDto.Builder().returnMessage(e.getMessage()).build();
            return new ResponseEntity<PurchaseReturnDto>(returnDto, HttpStatus.BAD_REQUEST);
        } catch (NullPointerException e){
            PurchaseReturnDto returnDto = new PurchaseReturnDto().builder().returnMessage(e.getMessage()).build();
            return new ResponseEntity<PurchaseReturnDto>(returnDto, HttpStatus.BAD_REQUEST);
        }

    }
}
