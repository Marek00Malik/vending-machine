package pl.mmalik.pragmatists.vending.machine.bo.builder;

import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;

/**
 * Coins Builder
 *
 * Created by Marek on 24.03.2016.
 */
public class CoinsBuilder {
    private Coins coins;

    public CoinsBuilder(){
        coins = new Coins();
    }

    public CoinsBuilder coinType(CoinType coinType) {
        this.coins.setCoinType(coinType);
        return this;
    }

    public CoinsBuilder count(int count) {
        this.coins.setCount(count);
        return this;
    }

    public Coins build() {
        return coins;
    }
}
