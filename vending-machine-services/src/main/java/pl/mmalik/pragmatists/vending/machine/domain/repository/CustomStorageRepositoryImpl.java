package pl.mmalik.pragmatists.vending.machine.domain.repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.mmalik.pragmatists.vending.machine.domain.model.QStorage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Custom Repository implementation.
 *
 * Created by Marek on 20.03.2016.
 */
@Component
public class CustomStorageRepositoryImpl implements CustomStorageRepository{

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public boolean findIfAvailableByItemId(Long itemId) {
        QStorage storage = QStorage.storage;
        BooleanExpression itemWithIdAndIsAvailable = storage.itemInStore.id.eq(itemId)
                .and(storage.itemsAvailable.gt(0));

        return new JPAQuery(em)
                .from(storage)
                .where(itemWithIdAndIsAvailable)
                .exists();
    }
}
