package pl.mmalik.pragmatists.vending.machine.bo.validate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change.ChangeReturnBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseRequestDto;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ClientWalletDoesNotHaveEnoughMoney;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ItemNotAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.exception.NotEnoughMoneyException;
import pl.mmalik.pragmatists.vending.machine.domain.repository.CustomStorageRepository;
import pl.mmalik.pragmatists.vending.machine.domain.repository.ItemRepository;

import java.math.BigDecimal;

import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.isBiggerThen;

/**
 * Created by Marek on 21.03.2016.
 */
@Component
public class ItemPurchaseValidator implements PurchaseValidator {

    @Autowired
    private ItemRepository repository;

    @Autowired
    private CustomStorageRepository storageRepository;

    @Autowired
    private ChangeReturnBoImpl changeCalculatorBo;

    @Override
    public void validate(PurchaseRequestDto purchaseDto) throws ItemNotAvailableException, ClientWalletDoesNotHaveEnoughMoney, NotEnoughMoneyException {
        Long itemId = purchaseDto.getItemId();

        BigDecimal itemCost = repository.findOne(itemId).getValueCost();

        BigDecimal clientTotalCash = purchaseDto.getClientWallet().getTotal();

        BigDecimal cashToReturn = clientTotalCash.subtract(itemCost);

        if (!storageRepository.findIfAvailableByItemId(itemId)) {
            throw new ItemNotAvailableException(itemId);
        }

        if (isBiggerThen(itemCost, clientTotalCash)) {
            throw new ClientWalletDoesNotHaveEnoughMoney(itemId, itemCost.subtract(clientTotalCash));
        }

        if (!changeCalculatorBo.canCollectChange(cashToReturn, purchaseDto.getClientWallet().getCoins())) {
            throw new NotEnoughMoneyException();
        }
    }
}
