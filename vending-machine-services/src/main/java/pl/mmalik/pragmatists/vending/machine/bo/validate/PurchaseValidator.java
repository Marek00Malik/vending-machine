package pl.mmalik.pragmatists.vending.machine.bo.validate;

import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseRequestDto;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ClientWalletDoesNotHaveEnoughMoney;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ItemNotAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.exception.NotEnoughMoneyException;

/**
 * Created by Marek on 20.03.2016.
 */
public interface PurchaseValidator {
    void validate(PurchaseRequestDto purchaseDto) throws ItemNotAvailableException, ClientWalletDoesNotHaveEnoughMoney, NotEnoughMoneyException;
}
