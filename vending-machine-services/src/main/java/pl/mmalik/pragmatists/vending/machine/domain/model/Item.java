package pl.mmalik.pragmatists.vending.machine.domain.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Marek on 19.03.2016.
 */
@Table
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column(name = "VALUE_COST")
    private BigDecimal valueCost;

    @Column
    private BigDecimal volume;

    @Column(name = "ITEM_ORDER",unique = true)
    private Integer item_order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValueCost() {
        return valueCost;
    }

    public void setValueCost(BigDecimal valueCost) {
        this.valueCost = valueCost;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public Integer getItem_order() {
        return item_order;
    }

    public void setItem_order(Integer item_order) {
        this.item_order = item_order;
    }
}
