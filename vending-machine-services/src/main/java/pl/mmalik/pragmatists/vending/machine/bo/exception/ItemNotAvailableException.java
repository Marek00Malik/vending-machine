package pl.mmalik.pragmatists.vending.machine.bo.exception;

/**
 * Created by Marek on 20.03.2016.
 */
public class ItemNotAvailableException extends Exception {
    private static final String MESSAGE_PREFIX = "Item with ID is not available. ITEM_ID = ";
    public ItemNotAvailableException(Long itemId) {
        super(MESSAGE_PREFIX + itemId);
    }
}
