package pl.mmalik.pragmatists.vending.machine.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mmalik.pragmatists.vending.machine.domain.model.Item;

/**
 * Created by Marek on 19.03.2016.
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{
}
