/**
 * Created by Marek on 17.03.2016.
 */
angular.module("app").directive('displayPanel', function () {

    return {
        templateUrl: 'displayPanel.html',
        restrict: 'E'
    };
});
