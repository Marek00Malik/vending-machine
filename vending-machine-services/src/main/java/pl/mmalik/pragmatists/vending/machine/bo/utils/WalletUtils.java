package pl.mmalik.pragmatists.vending.machine.bo.utils;

import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.FIFTY_CENTS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.FIVE_CENTS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.FIVE_EUROS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.ONE_CENT;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.ONE_EURO;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.TEN_CENTS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.TWENTY_CENTS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.TWO_CENTS;
import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.TWO_EUROS;

/**
 * Created by Marek on 21.03.2016.
 */
public final class WalletUtils {
    private WalletUtils(){}

    /**
     * @param wallet
     * @return
     *  return list of coin types that are possible to be used in wallet.
     */
    public static List<CoinType> getPossibleCoins(Wallet wallet) {
        return wallet.getCoins()
                .stream()
                .map(Coins::getCoinType)
                .sorted((o1, o2) -> Integer.compare(o1.ordinal(), o2.ordinal()))
                .collect(Collectors.toList());
    }

    /**
     * @param wallet
     * @return
     *  returns the smallest nominal coin from a wallet.
     */
    public static CoinType findSmallestValue(Wallet wallet){
        return getPossibleCoins(wallet)
                .stream()
                .sorted()
                .collect(Collectors.toList())
                .get(0);
    }

    /**
     * Creates new coin.
     * @param type
     * @param count
     * @return {@link Coins }
     */
    public static Coins createCoin(CoinType type, int count) {
        return Coins.builder()
                .coinType(type)
                .count(count)
                .build();
    }

    /**
     * Initialize Wallet with a given amount of money.
      * @param wallet
     */
    public static void initializeWallet(Wallet wallet) {
        wallet.addCoins(createCoin(ONE_CENT, 50));
        wallet.addCoins(createCoin(TWO_CENTS, 50));
        wallet.addCoins(createCoin(FIVE_CENTS, 30));
        wallet.addCoins(createCoin(TEN_CENTS, 50));
        wallet.addCoins(createCoin(TWENTY_CENTS, 20));
        wallet.addCoins(createCoin(FIFTY_CENTS, 15));
        wallet.addCoins(createCoin(ONE_EURO, 10));
        wallet.addCoins(createCoin(TWO_EUROS, 10));
        wallet.addCoins(createCoin(FIVE_EUROS, 10));
    }

    private static void append(BigDecimal total, BigDecimal toAdd){
        total = total.add(toAdd);
    }
}
