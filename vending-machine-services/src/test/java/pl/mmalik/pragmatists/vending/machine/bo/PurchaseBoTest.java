package pl.mmalik.pragmatists.vending.machine.bo;

import com.beust.jcommander.internal.Lists;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.ReflectionUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseRequestDto;
import pl.mmalik.pragmatists.vending.machine.bo.dto.PurchaseReturnDto;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ClientWalletDoesNotHaveEnoughMoney;
import pl.mmalik.pragmatists.vending.machine.bo.exception.ItemNotAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.exception.NotEnoughMoneyException;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.PurchaseBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change.ClientChangeCalculatorBoImpl;
import pl.mmalik.pragmatists.vending.machine.bo.validate.ItemPurchaseValidator;
import pl.mmalik.pragmatists.vending.machine.bo.validate.PurchaseValidator;
import pl.mmalik.pragmatists.vending.machine.domain.model.Item;
import pl.mmalik.pragmatists.vending.machine.domain.repository.ItemRepository;

import java.lang.reflect.Field;
import java.util.List;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.toBigDec;

/**
 * Created by Marek on 31.03.2016.
 */
public class PurchaseBoTest {

    @InjectMocks
    private PurchaseBoImpl sut;

    @Mock
    private ItemPurchaseValidator validator;

    @Mock
    private ItemRepository repository;


    private List<PurchaseValidator> purchaseValidators;
    private ClientChangeCalculatorBoImpl clientChange = new ClientChangeCalculatorBoImpl();
    private VendingMachineWallet machineWallet = new VendingMachineWallet();

    @BeforeMethod
    public void init(){
        MockitoAnnotations.initMocks(this);

        machineWallet.init();
        Field machineWalletField = ReflectionUtils.findField(ClientChangeCalculatorBoImpl.class, "vendingMachineWallet");
        machineWalletField.setAccessible(true);
        ReflectionUtils.setField(machineWalletField, clientChange, machineWallet);

        Field changeCalculatorField = ReflectionUtils.findField(PurchaseBoImpl.class, "changeCalculatorBo");
        changeCalculatorField.setAccessible(true);
        ReflectionUtils.setField(changeCalculatorField, sut, clientChange);

        purchaseValidators = Lists.newArrayList(validator);
        Field validatorsField = ReflectionUtils.findField(PurchaseBoImpl.class, "purchaseValidators");
        validatorsField.setAccessible(true);
        ReflectionUtils.setField(validatorsField, sut, purchaseValidators);
    }

    @Test(dataProvider = "correctData")
    public void testCorrectPurchase(Item item, PurchaseRequestDto requestDto, PurchaseReturnDto correct)
            throws ClientWalletDoesNotHaveEnoughMoney, NotEnoughMoneyException, ItemNotAvailableException {

        //when
        when(repository.findOne(anyLong())).thenReturn(item);


        PurchaseReturnDto purchaseReturnDto = sut.purchaseItem(requestDto);

        Assert.assertEquals(purchaseReturnDto.getReturnMessage(), correct.getReturnMessage());
        Assert.assertEquals(purchaseReturnDto.getCoins().size(), correct.getCoins().size());
    }

    @DataProvider(name = "correctData")
    public Object[][] getCorrectData(){
        return new Object[][]{
                {   item(2.49),
                    PurchaseRequestDto.builder()
                        .itemId(1l)
                        .clientWallet(wallet(createCoin(5, 1)))
                        .build(),
                    PurchaseReturnDto.builder()
                        .returnMessage("2.51")
                        .coins(Lists.newArrayList(
                            createCoin(2,1),
                            createCoin(.50, 1),
                            createCoin(.01, 1)
                    )).build()
                },
                {   item(3.21),
                        PurchaseRequestDto.builder()
                                .itemId(2l)
                                .clientWallet(
                                        wallet(createCoin(5, 1), createCoin(2,1), createCoin(1,1)))
                                .build(),
                        PurchaseReturnDto.builder()
                                .returnMessage("4.79")
                                .coins(Lists.newArrayList(
                                        createCoin(2,2),
                                        createCoin(.50, 1),
                                        createCoin(.2, 1),
                                        createCoin(.05,1),
                                        createCoin(0.02,2)
                                )).build()
                }
        };
    }

    private Item item(double value) {
        Item item = new Item();
        item.setValueCost(toBigDec(value));
        return item;
    }

    private Coins createCoin(double coinValue, int cnt) {
        return Coins.builder()
                .coinType(CoinType.findByValue(toBigDec(coinValue)))
                .count(cnt)
                .build();
    }

    private Wallet wallet(Coins...coins){
        Wallet newWallet = new Wallet();
        newWallet.addCoins(coins);
        return newWallet;
    }
}
