package pl.mmalik.pragmatists.vending.machine.bo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import pl.mmalik.pragmatists.vending.machine.web.json.deserializer.WalletJsonDeserializer;

import java.io.Serializable;

/**
 * Purchase Request dto.
 * Created by Marek on 20.03.2016.
 */
public class PurchaseRequestDto implements Serializable {
    private Long itemId;

    @JsonProperty("wallet")
    @JsonDeserialize(using = WalletJsonDeserializer.class)
    private Wallet clientWallet;

    public PurchaseRequestDto() {
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Wallet getClientWallet() {
        return clientWallet;
    }

    public void setClientWallet(Wallet clientWallet) {
        this.clientWallet = clientWallet;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private PurchaseRequestDto dto;

        public Builder() {
            this.dto = new PurchaseRequestDto();
        }

        public Builder clientWallet(Wallet clientWallet){
            this.dto.clientWallet = clientWallet;
            return this;
        }

        public Builder itemId(Long itemId){
            this.dto.itemId = itemId;
            return this;
        }

        public PurchaseRequestDto build(){
            return dto;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PurchaseRequestDto that = (PurchaseRequestDto) o;

        if (itemId != null ? !itemId.equals(that.itemId) : that.itemId != null)
            return false;
        return clientWallet != null ? clientWallet.equals(that.clientWallet) : that.clientWallet == null;

    }

    @Override
    public int hashCode() {
        int result = itemId != null ? itemId.hashCode() : 0;
        result = 31 * result + (clientWallet != null ? clientWallet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PurchaseRequestDto{" +
                "itemId=" + itemId +
                ", clientWallet=" + clientWallet +
                '}';
    }
}
