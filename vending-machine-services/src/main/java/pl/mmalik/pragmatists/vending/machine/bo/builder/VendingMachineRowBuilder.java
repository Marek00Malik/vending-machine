package pl.mmalik.pragmatists.vending.machine.bo.builder;

import pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage;
import pl.mmalik.pragmatists.vending.machine.bo.dto.VendingMachineRow;

import java.util.Map;

/**
 * Builder for VendingMachineRow.
 * Created by Marek on 24.03.2016.
 */
public class VendingMachineRowBuilder {
    private VendingMachineRow that;

    public VendingMachineRowBuilder() {
        that = new VendingMachineRow();
    }

    public VendingMachineRowBuilder rowNo(Character rowNo) {
        this.that.setRowNo(rowNo);
        return this;
    }

    public VendingMachineRowBuilder items(Map<Integer,ItemInStorage> items) {
        this.that.setItems(items);
        return this;
    }

    public VendingMachineRow build() {
        return that;
    }
}
