package pl.mmalik.pragmatists.vending.machine.bo.dto;

import com.google.common.collect.Sets;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;
import pl.mmalik.pragmatists.vending.machine.bo.builder.CoinsBuilder;
import pl.mmalik.pragmatists.vending.machine.bo.exception.CoinsStillAvailableException;
import pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

/**
 * Dto representing Wallet, filled with coins {@link Coins}.
 * <p>
 * Created by Marek on 20.03.2016.
 */
public class Wallet {
    private BigDecimal total = BigDecimal.ZERO;
    private Set<Coins> coins = Sets.newHashSet();

    public Wallet(){
        total = total.setScale(BigDecimalUtils.SCALE);
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Set<Coins> getCoins() {
        return coins;
    }

    public void setCoins(Set<Coins> coins) {
        this.coins = coins;
    }

    /**
     * Adds new {@link Coins} to wallet and increases its total value.
     *
     * @param coins that will be added to wallet.
     * @return increased total value of wallet.
     */
    public BigDecimal addCoins(Coins... coins) {
        Arrays.stream(coins)
                .forEach(newCoin -> addCoin(newCoin));
        return total;
    }

    /**
     * Take coins from wallet. This algorithm will only take as many coins of given {@link CoinType} as it is possible,
     * leaving the rest in the wallet.
     * @param maxPossibleCoinsCnt
     * @param {@link CoinType} coinType
     * @return Coins with the maximum possible amount of quantity to be taken from the wallet.
     */
    public Coins takeCoins(int maxPossibleCoinsCnt, CoinType coinType){
        CoinsBuilder coinsBuilder = Coins.builder().coinType(coinType);
        Optional<Coins> coinsOptional = coins.stream()
                .filter(coin -> coin.getCoinType().equals(coinType))
                .findFirst();

        if(coinsOptional.isPresent()){
            Coins remainingCoins = coinsOptional.get();
            int availableCnt = remainingCoins.getCount();
            if(availableCnt >= maxPossibleCoinsCnt){
                remainingCoins.setCount(availableCnt - maxPossibleCoinsCnt);
                Coins returningCoins = coinsBuilder
                        .count(maxPossibleCoinsCnt)
                        .build();

                subtractFromTotal(returningCoins);
                return returningCoins;
            } else {
                removeCoins(remainingCoins);
                return coinsBuilder
                        .count(availableCnt)
                        .build();
            }
        }

        return coinsBuilder
                .count(0)
                .build();
    }

    private BigDecimal addCoin(Coins coins) {
        Optional<Coins> first = getCoins()
                .stream()
                .filter(storedCoin -> storedCoin.equals(coins))
                .findFirst();

        if(first.isPresent()){
            first.get().add(coins.getCount());
        } else {
            getCoins().add(coins);
        }
        addToTotal(coins);

        return total;
    }

    /**
     * Subtract removing coins from total value of wallet.
     * @param coins
     */
    private void subtractFromTotal(Coins coins){
        total = total.subtract(coins.getCoinType().value().multiply(BigDecimal.valueOf(coins.getCount())));
    }

    /**
     * Removes coins from wallet.
     *
     * @param {@link Coins} coins
     */
    private void removeCoins(Coins coins) {
        if(coins.getCount() > 0){
            throw new CoinsStillAvailableException(coins);
        }
        subtractFromTotal(coins);
        getCoins().remove(coins);
    }

    /**
     * Adds new coins {@link Coins} to wallet.
     *
     * @param coins
     */
    private void addToTotal(Coins coins) {
        total = total.add(
                coins.getCoinType().value().multiply(BigDecimal.valueOf(coins.getCount())));
    }
}
