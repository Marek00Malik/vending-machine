angular.module("app").controller('VendingMachineController', function ($rootScope, $scope, $uibModal, $filter, AvailableItemsService, ItemPurchaseService, ProductImages, WalletSelectedCoinsJSFilter) {
    var defaultDisplayText = 'Choose Product';
    $scope.title = 'Welcome to Marks Vending Machine, You may buy any item that is in display below.';
    $scope.clientWallet = undefined;
    $scope.availableItems = undefined;
    $scope.itemRow = undefined;
    $scope.itemNo = undefined;
    $scope.itemId = undefined;
    $scope.textToDisplay = defaultDisplayText;

    var setWallet = function () {
        return {
            total: 0,
            coins: [
                {0.01: 0},
                {0.02: 0},
                {0.05: 0},
                {0.1: 0},
                {0.2: 0},
                {0.5: 0},
                {1: 0},
                {2: 0},
                {5: 0}
            ]
        };
    };

    var isItemSelected = function () {
        return $scope.itemNo !== undefined && $scope.itemRow !== undefined;
    };

    var areCoinsCollected = function () {
        return $scope.clientWallet.total > 0;
    };

    var onDisplay = function () {
        AvailableItemsService.getItems()
            .then(function (response) {
                var items = response.data;
                if (items !== undefined && items.length > 0) {
                    $scope.log('Items are available');
                    $scope.availableItems = items;
                    $scope.itemId = undefined;
                    $scope.itemRow = undefined;
                    $scope.itemNo = undefined;
                    $scope.clientWallet = setWallet();
                    $scope.textToDisplay = defaultDisplayText;
                }
            }, function (response) {
                $scope.log('Error when retrieving Items ' + response);
            });
    };

    var isReadyToBuy = function () {
        if (isItemSelected() && areCoinsCollected()) {
            buyItem();
        }
    };

    var buyItem = function () {
        //Buy item with client Money{totalAmount:x, clientChange:[{value:0.5, cnt:1},{value:1, cnt:2}]}
        ItemPurchaseService.buyItem($scope.itemId, $scope.clientWallet)
            .then(function (response) {
                $rootScope.log("buyItem method call buyItem : " + response.status);
                onDisplay();
                openReturnChangeModal(response.data);
            }, function (response) {
                alert(response.data.returnMessage);
            });
    };

    $scope.fillText = function () {
        if (isItemSelected()) {
            var itemsInRow = $filter('filter')($scope.availableItems, {rowNo: $scope.itemRow});
            var selectedItem = itemsInRow[0].items[$scope.itemNo];
            var itemValue = selectedItem.valueCost;
            $scope.itemId = selectedItem.id;
            $scope.textToDisplay = 'Item: ' + $scope.itemRow + $scope.itemNo + ' = ' + itemValue;
            isReadyToBuy();
        } else {
            $scope.textToDisplay = defaultDisplayText;
        }
    };

    $scope.findProductSrc = function (imgIdx) {
        return ProductImages.Products[imgIdx];
    };

    $scope.returnChange = function () {
        $scope.clientWallet = setWallet();
    };

    $scope.openInsertCoinsModal = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'enterCoinsModal.html',
            controller: 'EnterCoinsModalController',
            resolve: {
                wallet: function () {
                    return $scope.clientWallet;
                }
            }
        });

        modalInstance.result.then(function (wallet) {
            $scope.clientWallet = wallet;
            isReadyToBuy();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    var openReturnChangeModal = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'returnChangeModal.html',
            controller: 'ReturnChangeModalController',
            resolve: {
                coins: function () {
                    return data.coins;
                },
                returnMessage: function () {
                    return data.returnMessage;
                }
            }
        });
    };

    $scope.$watch('itemRow', function (newValue, oldValue) {
        if (oldValue !== undefined && newValue !== oldValue) {
            $scope.itemNo = undefined;
        }
    });

    $scope.$watch('itemNo', function (newValue, oldValue) {
        if (oldValue !== undefined && newValue !== oldValue) {
            $scope.itemRow = undefined;
        }
    });

    onDisplay();
});
