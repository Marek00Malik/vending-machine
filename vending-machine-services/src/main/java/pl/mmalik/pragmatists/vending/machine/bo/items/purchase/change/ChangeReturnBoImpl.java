package pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mmalik.pragmatists.vending.machine.bo.VendingMachineWallet;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;

import java.math.BigDecimal;
import java.util.Set;

import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.isBiggerThen;

/**
 * Validate Machines wallet for change return after purchase.
 * Created by Marek on 23.03.2016.
 */
@Component
public class ChangeReturnBoImpl extends AbstractChangeCalculator{

    private Wallet temporaryWallet;

    @Autowired
    private VendingMachineWallet vendingMachineWallet;

    /**
     * @param changeToCollect {@link BigDecimal} change that needs to be collected.
     * @param clientCoins {@link Set < Coins >} coins given by the client.
     * @return whether it is possible with vending machines wallet and clients input coins
     * to return the necessary change.
     */
    public boolean canCollectChange(BigDecimal changeToCollect, Set<Coins> clientCoins){
        temporaryWallet = new Wallet();
        Set<Coins> walletCoins = vendingMachineWallet.get().getCoins();
        temporaryWallet.addCoins(walletCoins.toArray(new Coins[walletCoins.size()]));
        temporaryWallet.addCoins(clientCoins.toArray(new Coins[clientCoins.size()]));

        super.calculateChange(changeToCollect);

        return isBiggerThen(getWallet().getTotal(), BigDecimal.ZERO);
    }

    @Override
    protected Wallet getWallet() {
        return temporaryWallet;
    }
}
