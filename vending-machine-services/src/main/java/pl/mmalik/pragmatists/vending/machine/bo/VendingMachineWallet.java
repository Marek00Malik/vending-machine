package pl.mmalik.pragmatists.vending.machine.bo;

import org.springframework.stereotype.Component;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;

import javax.annotation.PostConstruct;

import static pl.mmalik.pragmatists.vending.machine.bo.utils.WalletUtils.initializeWallet;

/**
 * Created by Marek on 21.03.2016.
 */
@Component
public class VendingMachineWallet {

    private Wallet wallet = new Wallet();

    @PostConstruct
    public void init(){
        initializeWallet(wallet);
    }

    public Wallet get(){
        return wallet;
    }
}
