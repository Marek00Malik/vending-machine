/**
 * Created by Marek on 15.03.2016.
 */
angular.module('app').constant('ProductImages', {
   Products:{
       1: '/img/product/coca-can.png',
       2: '/img/product/fanta-can.png',
       3: '/img/product/cherry_can.png',
       4: '/img/product/pepsi-can.png',
       
       5: '/img/product/tymbark-pomaranczowy.png',
       6: '/img/product/tymbark-jablko-mieta.png',
       7: '/img/product/woda-can.png',
       8: '/img/product/frugo-zielone.png',

       9: '/img/product/snickers.png',
       10: '/img/product/lays-papryka-chipsy.png',
       11: '/img/product/draze_kokosowe_korsarz.png',
       12: '/img/product/Kitkat.png'
   }
});
