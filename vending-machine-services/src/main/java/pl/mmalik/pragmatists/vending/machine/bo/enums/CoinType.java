package pl.mmalik.pragmatists.vending.machine.bo.enums;

import pl.mmalik.pragmatists.vending.machine.bo.exception.CoinTypeNotFound;
import pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.toBigDec;

/**
 * Enums representing monetary
 * Created by Marek on 20.03.2016.
 */
public enum CoinType {
    ONE_CENT(0.01),
    TWO_CENTS(0.02),
    FIVE_CENTS(0.05),
    TEN_CENTS(0.1),
    TWENTY_CENTS(0.2),
    FIFTY_CENTS(0.5),
    ONE_EURO(1),
    TWO_EUROS(2),
    FIVE_EUROS(5);

    private BigDecimal value;

    private CoinType(double value) {
        this.value = toBigDec(value);
    }

    public BigDecimal value() {
        return this.value;
    }

    public static CoinType findByValue(BigDecimal searchValue) {
        Optional<CoinType> coinTypeOptional = Arrays.stream(CoinType.values())
                .filter(coinType -> BigDecimalUtils.areEqual(coinType.value(), searchValue))
                .findFirst();

        if (!coinTypeOptional.isPresent()) {
            throw new CoinTypeNotFound(searchValue);
        }
        return coinTypeOptional.get();
    }
}
