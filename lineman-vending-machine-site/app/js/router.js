angular.module("app").config(function($routeProvider, $locationProvider) {

  $locationProvider.html5Mode({enabled:true});

  $routeProvider.when('/machine', {
    templateUrl: 'vendingMachine.html',
    controller: 'VendingMachineController'
  });

  $routeProvider.when('/admin', {
    templateUrl: 'adminPanel.html',
    controller: 'AdminController'
  });

  $routeProvider.when('/test', {
    templateUrl: 'test.html',
    controller: 'ModalDemoCtrl'
  });

  $routeProvider.otherwise({ redirectTo: '/machine' });
});
