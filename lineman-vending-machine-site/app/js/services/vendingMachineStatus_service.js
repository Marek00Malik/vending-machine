/**
 * Created by Marek on 31.03.2016.
 */
angular.module('app').factory('VendingMachineCheckService', function ($q, $http) {
    var getStatus = function () {
        return $http.get('/rest/admin/machine-check');
    };

    return {getStatus: getStatus};
});