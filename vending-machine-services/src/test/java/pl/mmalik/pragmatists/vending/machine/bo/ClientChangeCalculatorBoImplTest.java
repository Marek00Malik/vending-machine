package pl.mmalik.pragmatists.vending.machine.bo;

import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.util.ReflectionUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;
import pl.mmalik.pragmatists.vending.machine.bo.items.purchase.change.ClientChangeCalculatorBoImpl;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.*;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.toBigDec;

/**
 * Created by Marek on 21.03.2016.
 */
public class ClientChangeCalculatorBoImplTest {
    @InjectMocks
    private ClientChangeCalculatorBoImpl sut;

    @BeforeMethod
    public void intiMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(dataProvider = "correctResultData")
    public void testCorrectChange(BigDecimal change, List<Coins> correctReturn) {
        //given
        BigDecimal walletTotal = toBigDec(99.50);
        VendingMachineWallet walletMock = new VendingMachineWallet();
        walletMock.init();

        Field field = ReflectionUtils.findField(ClientChangeCalculatorBoImpl.class, "vendingMachineWallet");
        field.setAccessible(true);
        ReflectionUtils.setField(field, sut, walletMock);

        //when

        //then
        List<Coins> coins = sut.calculateChange(change);

        Assert.assertTrue(CollectionUtils.isEqualCollection(correctReturn, coins));


        walletTotal = walletTotal.subtract(change);
        Assert.assertEquals(walletMock.get().getTotal(), walletTotal);
    }

    @DataProvider(name = "correctResultData")
    public Object[][] correctResultData() {
        return new Object[][]{
                {toBigDec(1), Lists.newArrayList(fillCoin(ONE_EURO, 1))},
                {toBigDec(1.5), Lists.newArrayList(
                        fillCoin(ONE_EURO, 1),
                        fillCoin(FIFTY_CENTS, 1)
                )},
                {toBigDec(0.49), Lists.newArrayList(
                        fillCoin(TWENTY_CENTS, 2),
                        fillCoin(FIVE_CENTS, 1),
                        fillCoin(TWO_CENTS, 2)
                )},
                {toBigDec(3.74), Lists.newArrayList(
                        fillCoin(TWO_EUROS, 1),
                        fillCoin(ONE_EURO, 1),
                        fillCoin(FIFTY_CENTS, 1),
                        fillCoin(TWENTY_CENTS, 1),
                        fillCoin(TWO_CENTS, 2)
                )},
                {toBigDec(49), Lists.newArrayList(
                        fillCoin(FIVE_EUROS, 9),
                        fillCoin(TWO_EUROS, 2)
                )}
        };
    }

    private Coins fillCoin(CoinType type, int cnt) {
        return Coins.builder()
                .coinType(type)
                .count(cnt)
                .build();
    }
}
