package pl.mmalik.pragmatists.vending.machine.bo.exception;

/**
 * Created by Marek on 23.03.2016.
 */
public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException(){
        super("Not enough money in Vending Machine to return return change");
    }
}
