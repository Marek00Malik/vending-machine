package pl.mmalik.pragmatists.vending.machine.web.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Coins;
import pl.mmalik.pragmatists.vending.machine.bo.dto.Wallet;
import pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static pl.mmalik.pragmatists.vending.machine.bo.enums.CoinType.findByValue;
import static pl.mmalik.pragmatists.vending.machine.bo.utils.BigDecimalUtils.toBigDec;

/**
 * Class used for automatic deserialization from JSON to Wallet.
 *
 * @author Marek Malik, (mmalik@altkom.pl).
 */
public class WalletJsonDeserializer extends JsonDeserializer<Wallet> {
    @Override
    public Wallet deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Wallet wallet = new Wallet();
        JsonNode node = p.getCodec().readTree(p);

        JsonNode collectedCoins = node.get("collectedCoins");
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Integer>> coinsList = mapper.convertValue(collectedCoins, List.class);
        coinsList.stream()
                .forEach(
                        coinMap -> coinMap.entrySet()
                                .stream()
                                .forEach(coinEntry -> addCoin(coinEntry, wallet))
                );
        return wallet;
    }

    private void addCoin(Map.Entry<String, Integer> coinEntry, Wallet wallet) {
        CoinType coinType = findByValue(toBigDec(coinEntry.getKey()));
        Integer coinsCnt = coinEntry.getValue();

        Coins newCoin = Coins.builder()
                .coinType(coinType)
                .count(coinsCnt)
                .build();

        wallet.addCoins(newCoin);
    }
}
