angular.module("app").controller("AdminController", function ($scope, VendingMachineCheckService) {
    // because the stubbed endpoint returns an array of results, .query() is used
    // if the endpoint returned an object, you would use .get()
    $scope.total = 0;

    var getMachineDetails = function () {
        VendingMachineCheckService.getStatus()
            .then(function (response) {
                $scope.log('Admin Panel, check vending machine status');

                $scope.total = response.data.wallet.total;
                $scope.machineStatus = true;
                $scope.machineStatusMessage = "Up And Running";
            }, function () {
                $scope.machineStatus = false;
                $scope.machineStatusMessage = "Machine is down!!!!";
            });
    };
    getMachineDetails();
});
