/**
 * Created by Marek on 15.03.2016.
 */
angular.module('app').factory('ItemPurchaseService', function ($filter, $q, $http, WalletSelectedCoinsJSFilter) {
    var buyItem = function (itemId, clientWallet) {
        var coins = WalletSelectedCoinsJSFilter.filter(clientWallet.coins);

        return $http.post('/rest/purchase-item',
            {
                'itemId': itemId,
                'wallet': {
                    'collectedCoins': coins
                }
            }
        );

    };

    return {buyItem: buyItem};
});