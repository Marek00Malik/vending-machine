package pl.mmalik.pragmatists.vending.machine.domain.repository;

/**
 * Custom repository for Custom Repository.
 * Created by Marek on 20.03.2016.
 */
public interface CustomStorageRepository {
    /**
     * @param itemId
     * @return returns true if Item with id is available in stock.
     */
    boolean findIfAvailableByItemId(Long itemId);
}
