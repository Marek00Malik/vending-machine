package pl.mmalik.pragmatists.vending.machine.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage;
import pl.mmalik.pragmatists.vending.machine.domain.model.Storage;

import java.util.List;

/**
 * Created by Marek on 19.03.2016.
 */
@Repository
public interface StorageRepository extends JpaRepository<Storage, Long>, QueryDslPredicateExecutor<Storage> {

    /**
     * @return items which are still stored in the magazine.
     */
    @Query("SELECT new pl.mmalik.pragmatists.vending.machine.bo.dto.ItemInStorage(i.id, i.name, i.valueCost, i.volume, i.item_order, storage.itemsAvailable) " +
            "FROM #{#entityName} storage join storage.itemInStore i " +
            "WHERE storage.itemsAvailable > 0")
    List<ItemInStorage> getStoredItems();

}
