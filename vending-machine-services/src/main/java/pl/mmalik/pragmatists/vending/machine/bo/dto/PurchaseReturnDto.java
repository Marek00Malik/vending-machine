package pl.mmalik.pragmatists.vending.machine.bo.dto;

import com.beust.jcommander.internal.Lists;

import java.util.List;

/**
 * Dto to be returned after purchase.
 * Created by Marek on 23.03.2016.
 */
public class PurchaseReturnDto {
    private List<Coins> coins = Lists.newArrayList();
    private String returnMessage;

    public List<Coins> getCoins() {
        return coins;
    }

    public void setCoins(List<Coins> coins) {
        this.coins = coins;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder{
        private PurchaseReturnDto dto = new PurchaseReturnDto();

        public Builder returnMessage(String message){
            this.dto.returnMessage = message;
            return this;
        }

        public Builder coins(List<Coins> coins){
            this.dto.coins = coins;
            return this;
        }

        public PurchaseReturnDto build(){
            return dto;
        }
    }
}
